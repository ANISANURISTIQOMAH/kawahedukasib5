package id.kedukasi.controller;

import id.kedukasi.model.DataItem;
import io.vertx.core.json.JsonObject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Path("/item")
public class DataItemController {

    // post/create data
    // http://localhost:9090/item
    @POST
    @Transactional
    public Response create(JsonObject request){
        DataItem item = new DataItem();

        item.name = request.getString("name");
        item.count = request.getInteger("count");
        item.price = request.getInteger("price");
        item.type = request.getString("type");
        item.description = request.getString("description");
        item.createdAt = LocalDate.parse(request.getString("createdAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        item.updatedAt = LocalDate.parse(request.getString("updatedAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        item.persist();

        return Response.ok().entity(new HashMap<>()).build();
    }

    //get all data
    @GET
    public Response list(){
        return Response.ok().entity(DataItem.listAll()).build();
    }
    // get data from id
    @GET
    @Path("/{id}")
    public Response getById(@PathParam("id") Integer id){
        DataItem item = DataItem.findById(id);
        if(item == null){
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Map.of("message", "PESERTA_NOT_FOUND"))
                    .build();
        }
        return Response.ok().entity(item).build();

    }

    // delete data by id
    @DELETE
    @Path("/{id}")
    @Transactional
    public Response delete(@PathParam("id") Integer id){
        DataItem.deleteById(id);
        return  Response.ok().entity(new HashMap<>()).build();
    }

    //update data by id
    @PUT
    @Path("/{id}")
    @Transactional
    public Response update(@PathParam("id") Integer id, JsonObject request){
        DataItem item = DataItem.findById(id);
        item.name = request.getString("name");
        item.count = request.getInteger("count");
        item.price = request.getInteger("price");
        item.type = request.getString("type");
        item.description = request.getString("description");
        item.createdAt = LocalDate.parse(request.getString("createdAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        item.updatedAt = LocalDate.parse(request.getString("updatedAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        //save
        item.persist();
        return Response.ok().entity(new HashMap<>()).build();



    }


}
