package id.kedukasi.controller;



import id.kedukasi.model.dto.UploadDataItem;
import id.kedukasi.service.ExcelService;
import id.kedukasi.service.ItemService;
import id.kedukasi.service.ReportService;
import id.kedukasi.service.ScheduleService;
import io.vertx.core.json.JsonObject;
import net.sf.jasperreports.engine.JRException;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/item")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DataItemController {

    @Inject
    ItemService itemService;

    @Inject
    ExcelService excelService;

    @Inject
    ScheduleService scheduleService;

    @Inject
    ReportService reportService;

    @GET
    @Path("/report")
    @Produces("application/pdf")
    public Response create() throws JRException {
        return reportService.exportJasper();
    }

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(@MultipartForm UploadDataItem request) throws IOException {
        return excelService.upload(request);
    }

    // post/create data
    @POST
    @Transactional
    public Response list(JsonObject request){
        return itemService.create(request);
    }

    //get all data
    @GET
    public Response list(){
        return itemService.list();
    }
    // get data from id

    @GET
    @Path("/{id}")
    public Response getById(@PathParam("id") Integer id) {
        return itemService.getById(id);
    }

    // delete data by id
    @DELETE
    @Path("/{id}")
    @Transactional
    public Response delete(@PathParam("id") Integer id){
        return itemService.delete(id);
    }

    //update data by id
    @PUT
    @Path("/{id}")
    @Transactional
    public Response update(@PathParam("id") Integer id, JsonObject request){
        return itemService.update(id, request);

    }


}
