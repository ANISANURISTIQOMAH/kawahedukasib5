package id.kedukasi.service;

import id.kedukasi.model.DataItem;
import io.quarkus.scheduler.Scheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;


import java.util.List;

import static java.lang.Integer.getInteger;

@ApplicationScoped
public class ScheduleService {

    Logger logger = LoggerFactory.getLogger(ScheduleService.class);

    @Inject
    EntityManager entityManager;
//    test sccheduled
//    @Scheduled(every = "5s")
//    public void  generateKawahedukasi(){
//        logger.info("kawahedukasi");
//    }

    // delete tiap satu jam

    @Scheduled(every = "1h")
    public void deleteZeroItem() {
        String queryString = "SELECT * FROM batch_5.item WHERE \"count\" = 0";
        Query query = entityManager.createNativeQuery(queryString, DataItem.class);
        List<DataItem> list = query.getResultList();
        deleteItem(list);
        logger.info("Total Item Deleted");
    }

    @Transactional
    public void deleteItem(List<DataItem> items) {
        for (DataItem item : items) {
            item.delete();
        }

    }

}

