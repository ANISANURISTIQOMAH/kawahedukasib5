package id.kedukasi.service;

import id.kedukasi.model.DataItem;
import id.kedukasi.model.dto.UploadDataItem;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@ApplicationScoped
public class ExcelService {

    public Response upload(UploadDataItem request) throws IOException {
        List<DataItem> dataItemList = new ArrayList<>();

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(request.file);
        XSSFWorkbook workbook = new XSSFWorkbook(byteArrayInputStream);
        XSSFSheet sheet = workbook.getSheetAt(0);

        //remove header
        sheet.removeRow(sheet.getRow(0));

        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            DataItem item = new DataItem();

            Row row = rowIterator.next();
            item.name =row.getCell(0).getStringCellValue();
            item.count = Double.valueOf(row.getCell(1).getNumericCellValue()).intValue();
            item.price =  Double.valueOf(row.getCell(2).getNumericCellValue()).intValue();
            item.type =row.getCell(3).getStringCellValue();
            item.description = row.getCell(4).getStringCellValue();
//            item.createdAt = LocalDate.parse(row.getCell(5).getStringCellValue(), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
//            item.updatedAt = LocalDate.parse(row.getCell(6).getStringCellValue(), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        }
        persistListDataItem(dataItemList);

        return  Response.ok().build();

    }

    @Transactional
    @TransactionConfiguration(timeout = 30)
    public void persistListDataItem(List<DataItem> dataItemList){
        DataItem.persist(dataItemList);
    }
}
