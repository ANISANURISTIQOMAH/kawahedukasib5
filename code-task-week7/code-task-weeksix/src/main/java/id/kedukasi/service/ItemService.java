package id.kedukasi.service;


import id.kedukasi.model.DataItem;
import io.vertx.core.json.JsonObject;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class ItemService {
    //untuk post
    @Transactional
    public Response create(JsonObject request){
        String name = request.getString("name");
        Integer count = request.getInteger("count");
        Integer price = request.getInteger("price");
        String type = request.getString("type");
        String description = request.getString("description");
        LocalDate createdAt = LocalDate.parse(request.getString("createdAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LocalDate updatedAt = LocalDate.parse(request.getString("updatedAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));


        if(name == null || count == null || price == null || type == null || description == null || createdAt == null || updatedAt == null){
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(Map.of("message", "BAD_REQUEST"))
                    .build();
        }

        DataItem item = new DataItem();

        item.name = request.getString("name");
        item.count = request.getInteger("count");
        item.price = request.getInteger("price");
        item.type = request.getString("type");
        item.description = request.getString("description");
        item.createdAt = LocalDate.parse(request.getString("createdAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        item.updatedAt = LocalDate.parse(request.getString("updatedAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        item.persist();

        return Response.ok().entity(new HashMap<>()).build();

    }

    //untuk get by id
    @Transactional
    public Response getById(@PathParam("id") Integer id){
        DataItem item = DataItem.findById(id);
        if(item == null){
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Map.of("message", "PESERTA_NOT_FOUND"))
                    .build();
        }
        return Response.ok().entity(item).build();

    }

    //untuk delete by id
    @Transactional
    public Response delete(@PathParam("id") Integer id){
        DataItem item = DataItem.findById(id);
        if(item == null){
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Map.of("message", "ITEM_ALREADY_DELETED"))
                    .build();
        }
        item.delete();
        return Response.ok().entity(item).build();
    }

    //untuk get all
    @Transactional
    public Response list(){
        return Response.ok().entity(DataItem.listAll()).build();
    }


    //untuk put update by id
    @Transactional
    public Response update(Integer id, JsonObject request){
        String name = request.getString("name");
        Integer count = request.getInteger("count");
        Integer price = request.getInteger("price");
        String type = request.getString("type");
        String description = request.getString("description");
        LocalDate createdAt = LocalDate.parse(request.getString("createdAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LocalDate updatedAt = LocalDate.parse(request.getString("updatedAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));


        if(name == null || count == null || price == null || type == null || description == null || createdAt == null || updatedAt == null){
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(Map.of("message", "BAD_REQUEST"))
                    .build();
        }
        DataItem item = DataItem.findById(id);
        item.name = request.getString("name");
        item.count = request.getInteger("count");
        item.price = request.getInteger("price");
        item.type = request.getString("type");
        item.description = request.getString("description");
        item.createdAt = LocalDate.parse(request.getString("createdAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        item.updatedAt = LocalDate.parse(request.getString("updatedAt"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        //save
        item.persist();
        return Response.ok().entity(new HashMap<>()).build();

    }
}
