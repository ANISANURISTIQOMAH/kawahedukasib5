package id.kedukasi.model.dto;

import javax.ws.rs.FormParam;

public class UploadDataItem {
    @FormParam("file")
    public byte[] file;
}
