package id.kedukasi.model;


import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "item")
public class DataItem extends PanacheEntityBase {
    @Id
    @SequenceGenerator(
            name = "itemSequence",
            sequenceName = "item_sequence",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(generator = "itemSequence", strategy = GenerationType.SEQUENCE)

    @Column(name = "id")
    public Integer id;

    @Column(name = "name", nullable = false, length = 10)
    public String name;

    @Column(name = "count", nullable = false, length = 10)
    public Integer count;

    @Column(name = "price", nullable = false, length = 10)
    public Integer price;


    @Column(name = "type",nullable = false, length = 10 )
    public String type;


    @Column(name = "description", nullable = false, length = 10)
    public String description;


    @CreationTimestamp
    @Column(name = "createdAt")
    public LocalDate createdAt;


    @UpdateTimestamp
    @Column(name = "updatedAt")
    public LocalDate updatedAt;

}
